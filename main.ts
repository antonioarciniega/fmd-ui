import {app, BrowserWindow, Menu, screen} from 'electron';
import * as path from 'path';
import * as url from 'url';

let win, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');


/// create a global var, wich will keep a reference to out loadingScreen window
let loadingScreen;
const createLoadingScreen = () => {
	/// create a browser window
	loadingScreen = new BrowserWindow(
		Object.assign({
			/// define width and height for the window
			width: 400,
			height: 320,
			/// remove the window frame, so it will become a frameless window
			frame: false,
			center: true,
			/// and set the transparency, to remove any window background color
			transparent: true
		})
	);
	loadingScreen.setResizable(false);
	loadingScreen.loadURL(
		'file://' + __dirname + '/loading.html'
	);
	loadingScreen.on('closed', () => (loadingScreen = null));
	loadingScreen.webContents.on('did-finish-load', () => {
		loadingScreen.show();
	});
};


function createWindow() {

	const electronScreen = screen;
	const size = electronScreen.getPrimaryDisplay().workAreaSize;

	// Create the browser window.
	win = new BrowserWindow({
		width: 980,
		height: 800,
		center: true,
		resizable: false,
		frame: false,
		webPreferences: {
			nodeIntegration: true,
			webSecurity: false
		},
		show: false
	});

	Menu.setApplicationMenu(null);

	if (serve) {
		require('electron-reload')(__dirname, {
			electron: require(`${__dirname}/node_modules/electron`)
		});
		win.loadURL('http://localhost:4200');
	} else {
		win.loadURL(url.format({
			pathname: path.join(__dirname, 'dist/index.html'),
			protocol: 'file:',
			slashes: true
		}));
	}


	if (serve) {
		win.webContents.openDevTools();
	}

	// Emitted when the window is closed.
	win.on('closed', () => {
		// Dereference the window object, usually you would store window
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		win = null;
	});

	// /// keep listening on the did-finish-load event, when the mainWindow content has loaded
	win.webContents.on('did-finish-load', () => {
		/// then close the loading screen window and show the main window
		if (loadingScreen) {
			loadingScreen.close();
		}
		win.show();
	});

}

try {

	// This method will be called when Electron has finished
	// initialization and is ready to create browser windows.
	// Some APIs can only be used after this event occurs.
	app.on('ready', () => {
		createLoadingScreen();
		createWindow();
	});

	// Quit when all windows are closed.
	app.on('window-all-closed', () => {
		// On OS X it is common for applications and their menu bar
		// to stay active until the user quits explicitly with Cmd + Q
		if (process.platform !== 'darwin') {
			app.quit();
		}
	});

	app.on('activate', () => {
		// On OS X it's common to re-create a window in the app when the
		// dock icon is clicked and there are no other windows open.
		if (win === null) {
			createWindow();
		}
	});

} catch (e) {
	// Catch Error
	// throw e;
}
