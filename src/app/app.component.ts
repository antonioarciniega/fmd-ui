import {Component} from '@angular/core';
import {ElectronService} from './core/services';
import {TranslateService} from '@ngx-translate/core';
import {AppConfig} from '../environments/environment';
import {Color, Titlebar} from 'custom-electron-titlebar';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	titleBar;
	appVersion;

	constructor(
		public electronService: ElectronService,
		private translate: TranslateService
	) {
		translate.setDefaultLang('en');
		console.log('AppConfig', AppConfig);
		this.appVersion = this.electronService.getAppVersion();

		if (electronService.isElectron) {
			console.log(process.env);
			console.log('Mode electron');
			console.log('Electron ipcRenderer', electronService.ipcRenderer);
			console.log('NodeJS childProcess', electronService.childProcess);

			this.titleBar = new Titlebar({
				backgroundColor: Color.fromHex('#001b29'),
				overflow: 'hidden',
				titleHorizontalAlignment: 'left',
			});
		} else {
			console.log('Mode web');
		}
	}
}
