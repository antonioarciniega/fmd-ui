import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './pages/main/main.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [MainComponent],
  imports: [
	CommonModule,
	SharedModule,
	SettingsRoutingModule
  ]
})
export class SettingsModule { }
